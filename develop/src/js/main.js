'use strict';

// flying menu
var header = document.getElementById('page-header');
window.addEventListener('scroll', scrollMe);

function scrollMe() {
    if (window.scrollY > 675) {
        header.style.opacity = 1;
        header.style.position = "fixed";

    } else if (window.scrollY < 675) {
        header.style.position = "absolute";
    }
}

// carousel circles switcher
var carouselCircles = document.getElementsByClassName('carousel-control__item');
carouselSwitcher.bind(this);
for (let sibling of carouselCircles) {
    sibling.addEventListener('click', carouselSwitcher)
}

function carouselSwitcher() {
    if (this.classList.contains('active')) {
        return null;
    }
    var parent = this.parentNode;
    for (let it of parent.children) {
        if (it.classList.contains("active")) {
            it.classList.remove("active");
        }
    }
    this.classList.add("active");
}
// carousel portrait switcher
var nextbutton = document.getElementById('carousel-next');
var prevbutton = document.getElementById('carousel-prev');
nextbutton.addEventListener('click', nextPortrait);
prevbutton.addEventListener('click', prevPortrait);

function nextPortrait() {
    var currentPortrait = document.getElementsByClassName('carousel-lister__item active');
    currentPortrait = currentPortrait[0];
    currentPortrait.classList.toggle("active");
    if (!currentPortrait.nextElementSibling) {
        currentPortrait.parentElement.firstElementChild.classList.toggle('active');
    } else {
        currentPortrait.nextElementSibling.classList.toggle('active');
    }
}

function prevPortrait() {
    var currentPortrait = document.getElementsByClassName('carousel-lister__item active');
    currentPortrait = currentPortrait[0];
    currentPortrait.classList.toggle("active");
    if (!currentPortrait.previousElementSibling) {
        currentPortrait.parentElement.lastElementChild.classList.toggle('active');
    } else {
        currentPortrait.previousElementSibling.classList.toggle('active');
    }
}

//video story
var videoframe = document.getElementById('video-story'),
    playButton = document.getElementById('story-controls__button');
videoframe.addEventListener('pause', function (e) {
    e.target.style = "";
})
playButton.addEventListener('click', playVideo);

function playVideo() {
    videoframe.play();
    videoframe.style.zIndex = 9999;
    videoframe.setAttribute('controls', 'controls');
}

// preloader
window.addEventListener('load', function () {
    var preloader = document.getElementById('preloader');
    setTimeout(function () {
        preloader.style.display = "none";
            preloader.parentElement.removeChild(preloader);
    }, 2000);
});
